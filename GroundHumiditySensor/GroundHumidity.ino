/*
Basic Script for reading a ground humidity sensor and pusblish the data via
MQTT.

Sensor Hardware: https://de.aliexpress.com/item/32841582669.html?spm=a2g0o.detail.1000060.2.1c5cdcc9I9h4cl&gps-id=pcDetailBottomMoreThisSeller&scm=1007.13339.99734.0&scm_id=1007.13339.99734.0&scm-url=1007.13339.99734.0&pvid=36489718-0ea0-47ea-b71c-918ab6fded7a

Libaries:
- ESP32 Module Libaray
- PubSubClient
- DTH-ESP
- EZTime (read time from web server)

Note:
- Ground Humidity Thresholds may sitll wrong

Fully configuration via variables in the Configuration section.

Author: Peter Zechel M.Sc.

License: GNU General Public License)
*/


// Nessasarry Libaries

#include "WiFi.h"
#include <PubSubClient.h>
#include <DHTesp.h>
#include <ezTime.h>
/* #############################################
  Configuration
   ############################################# */

#define DHTPIN    22 //Pin for DHT
#define DHTTYPE   DHT11 // DHT Type
#define GROUND_HUMIDITYPIN    32 // Humidity Pin
#define SENSORNAME "SensorHochbeet" //Name for MQTT
#define MINUTS_FOR_DEEPSLEEP 60 // Minutes in deep sleep after sensor value pusblish


   /* WIFI and MQTT*/
const char* SSID = "-"; //SSID
const char* PSK = "-"; //PW
const char* MQTT_BROKER = "192.168.178.-"; //IP of MQTT Broker
#define MQTT_PORT 1883 // MQTT PORT


/* CHANNELS */
#define MQTT_TIME_STEP_KEY "SensorHochbeet/updated"
#define MQTT_GOURND_HUMIDITY_KEY "SensorHochbeet/bodenfeuchte"
#define MQTT_TEMPERATURE_KEY "SensorHochbeet/temperatur"
#define MQTT_HUMIDITY_KEY "SensorHochbeet/feuchtigkeit"


/* Sensor */

#define MIN_GR_HUM_VALUE 1100.0 //<--- in Water
#define MAX_GR_HUM_VALUE 3300.0 //<--- in Air

/* #############################################
  Variables
   ############################################# */

WiFiClient espClient;
PubSubClient client(espClient);
DHTesp dht;

int dhtPin = DHTPIN;
long lastMsg = 0;
char msg[50];
int value = 0;



/* #############################################
  Local function prototypes
   ############################################# */

void setup_wifi();
void callback(char* topic, byte* payload, unsigned int length);
void reconnect();
/* #############################################
  Additional Setup Functions
   ############################################# */

void setup_wifi() {
    delay(10);
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(SSID);
    //WiFi.hostname(SENSORNAME);
    WiFi.begin(SSID, PSK);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

}


/* #############################################
  Additional Work Functions
   ############################################# */

void callback(char* topic, byte* payload, unsigned int length) {
    int i;

    // Copy data to string
    char msg[length+1];
    for (int i = 0; i < length; i++) {
        msg[i] = (char)payload[i];
    }
    msg[length] = '\0';

    // Print some debug
    Serial.print("Received message [");
    Serial.print(topic);
    Serial.print("]: [");
    Serial.print(msg);
    Serial.println("]");


}

void reconnect() {
    while (!client.connected()) {
        Serial.println("Reconnecting MQTT...");
        if (!client.connect(SENSORNAME)) {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" retrying in 5 seconds");
            delay(5000);
        }
    }
    Serial.println("MQTT Connected...");
}

float convertAnalogToGroundHumidity(int value){
  float fvalue = (float)value;
  float fhum;

  fhum = (1- (fvalue - MIN_GR_HUM_VALUE)/(MAX_GR_HUM_VALUE - MIN_GR_HUM_VALUE))*100;
  return fhum;
}

/* #############################################
  Arduino typical Function
   ############################################# */

int timezone = 1;
void setup() {
  int errorCounter = 0;


    Serial.begin(115200);
    Serial.println("");
    Serial.println("");
    Serial.println("Hello im here...");


    pinMode(DHTPIN, INPUT_PULLUP);
    pinMode(GROUND_HUMIDITYPIN, INPUT);
    Serial.println("Init of hardware complete....");
    Serial.print("calibrating sensor ");


    // Initialize temperature sensor
    dht.setup(dhtPin, DHTesp::DHTTYPE);
    delay(dht.getMinimumSamplingPeriod());

    //setup wifi
    setup_wifi();

    // setup mqtt
    client.setServer(MQTT_BROKER, MQTT_PORT);
    client.setCallback(callback);
    //connet mqtt
    reconnect();
    waitForSync();

    // Setup Timezone server
    Timezone myTZ;

    myTZ.setLocation(F("Europe/Berlin"));

    // Read sensor data
    Serial.print("reading sensor - ");
    float humidity = dht.getHumidity();
    float temperature = dht.getTemperature();
    int analog_groundHumidity = analogRead(GROUND_HUMIDITYPIN);
    float groundHumidity = convertAnalogToGroundHumidity(analog_groundHumidity);

    // Print data
    Serial.println("Time:      " + myTZ.dateTime(COOKIE));
    Serial.print("Temperature: ");
    Serial.print(temperature);
    Serial.print(" - Air Humidity: ");
    Serial.print(humidity);
    Serial.print(" - Ground Humidity: ");
    Serial.println(groundHumidity);



    // Publish data to mqtt
    client.publish(MQTT_TIME_STEP_KEY, String(myTZ.dateTime(COOKIE)).c_str(), true);
    client.publish(MQTT_GOURND_HUMIDITY_KEY, String(groundHumidity).c_str(), true);
    client.publish(MQTT_TEMPERATURE_KEY, String(temperature).c_str(), true);
    client.publish(MQTT_HUMIDITY_KEY, String(humidity).c_str(), true);
    delay(1000);



    Serial.println("Disconnecting to MQTT and WIFI");
    client.disconnect();
    WiFi.disconnect();

    Serial.print("Going into deep sleep for ");
    Serial.print(MINUTS_FOR_DEEPSLEEP);
    Serial.println(" minutes");
    ESP.deepSleep(MINUTS_FOR_DEEPSLEEP * 60 * 1000000);
    delay(100);
}

void loop() {
  // Nothing todo here,
}
